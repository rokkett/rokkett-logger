mod encoding;

use serde::{Deserialize, Serialize};
use std::fmt::Display;

/// All requests are wrapped in this struct.
#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct HeaderPackage {
	pub bytes: usize,
}

impl Into<Vec<u8>> for HeaderPackage {
	fn into(self) -> Vec<u8> {
		match encoding::serialize(&self) {
			Ok(bytes) => {
				let mut padded = vec![0u8; 8];
				padded[..bytes.len()].copy_from_slice(&bytes);
				padded
			},
			Err(err) => {
				eprintln!("could not serialize {self:?}\nerror = {err}");
				vec![]
			}
		}
	}
}

impl HeaderPackage {
	pub fn try_parse(bytes: &Vec<u8>) -> Option<Self> {
		encoding::deserialize(bytes).ok()
	}
}

/// All requests are wrapped in this struct.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct RequestPackage {
	/// A command indicates the requested action by the sender (client) to which the receiver (server) will react to.
	pub command: Command,
}

impl Into<Vec<u8>> for RequestPackage {
	fn into(self) -> Vec<u8> {
		match encoding::serialize(&self) {
			Ok(bytes) => bytes,
			Err(err) => {
				eprintln!("could not serialize {self:?}\nerror = {err}");
				vec![]
			}
		}
	}
}

impl From<Vec<u8>> for RequestPackage {
	fn from(bytes: Vec<u8>) -> Self {
		match encoding::deserialize::<RequestPackage>(&bytes) {
			Ok(value) => value,
			Err(err) => {
				eprintln!("could not deserialize bytes to RequestPackage\nerror = {err}");
				RequestPackage { command: Command::NoOp }
			}
		}
	}
}

/// All requests are wrapped in this struct.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ResponsePackage {
	/// When responding to a request, this attribute indicates the success or failure of the command.
	pub status: Status,
}

impl Into<Vec<u8>> for ResponsePackage {
	fn into(self) -> Vec<u8> {
		match encoding::serialize(&self) {
			Ok(bytes) => bytes,
			Err(err) => {
				eprintln!("could not serialize {self:?}\nerror = {err}");
				vec![]
			}
		}
	}
}

impl From<Vec<u8>> for ResponsePackage {
	fn from(bytes: Vec<u8>) -> Self {
		match encoding::deserialize::<ResponsePackage>(&bytes) {
			Ok(value) => value,
			Err(err) => {
				eprintln!("could not deserialize bytes to ResponsePackage\nerror = {err}");
				ResponsePackage {
					status: Status::ParseErr,
				}
			}
		}
	}
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum Status {
	/// The command requested by the sender (client) was executed successfully.
	Ok,

	/// The request was received and parsed successfully, but no action was be taken.
	/// Usually this means that no command was issued by the sender (client).
	NoCmd,

	/// Unclassifiable error including a error message.
	Err(String),

	/// The payload could not be parsed correctly by the receiver (server).
	ParseErr,

	/// The received command requires authentication or authorization, which was not provided or invalid.
	AuthErr,

	/// Indicates that the sender (client) has sent too many requests.
	RateLimit,

	/// The payload was too large and thus not parsed.
	TooLarge,

	/// The receiver (server) has blocked the request and will most likely close the connection.
	/// This will happen if the sender (client) has sent too many requests to which the receiver (server) has reponded with an error.
	Blocked,
}

impl Display for Status {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum Command {
	/// No operation.
	NoOp,

	/// Authenticate and authorize the sender (client).
	Login(AuthToken),

	/// Process a log message.
	Log(LogMessage),
}

/// Used to authenticate and authorize a sender (client).
pub type AuthToken = String;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct LogMessage {
	/// The name of the service which produced the log message.
	pub service: String,

	/// The log target inside the application.
	pub target: String,

	/// The log level of the log message.
	pub level: String,

	/// Timestamp in form of a RFC 3339 or ISO 8601 datetime string.
	pub timestamp: String,

	/// The source of the log message. Usually in the format of <source file>:<line number>.
	pub source: String,

	/// The actual log message.
	pub message: String,
}

impl LogMessage {
	/// Create log message from log record.
	///
	/// Requires 'client' feature (enabled by default).
	#[cfg(all(feature = "chrono", feature = "log"))]
	pub fn from_log_record(record: &log::Record, service: &str) -> Self {
		LogMessage {
			service: service.to_owned(),
			target: record.target().to_string(),
			level: record.level().to_string(),
			timestamp: chrono::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, true),
			source: format!("{}:{}", record.file().unwrap_or("-"), record.line().unwrap_or(0)),
			message: record.args().to_string(),
		}
	}
}

impl Display for LogMessage {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{ts} {lvl} [{srv}] ({trg}@{src}) {msg}",
			ts = self.timestamp,
			srv = self.service,
			lvl = self.level,
			trg = self.target,
			src = self.source,
			msg = self.message
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	#[cfg(all(feature = "chrono", feature = "log"))]
	fn test_from_log_record_log_message() {
		let service = "service";
		let target = "target";
		let level = "DEBUG";
		let source_file = file!();
		let source_line = line!();
		let source = format!("{source_file}:{source_line}");

		let record = log::Record::builder()
			.target(target)
			.level(log::Level::Debug)
			.file(Some(source_file))
			.line(Some(source_line))
			.args(format_args!("foo bar baz"))
			.build();

		let actual = LogMessage::from_log_record(&record, service);

		assert_eq!(service, actual.service);
		assert_eq!(target, actual.target);
		assert_eq!(level, actual.level);
		assert_eq!(source, actual.source);
		assert_eq!("foo bar baz", actual.message);
	}

	#[test]
	fn test_display_log_message() {
		let actual: String = format!(
			"{}",
			LogMessage {
				service: "service".to_string(),
				target: "target".to_string(),
				level: "level".to_string(),
				timestamp: "timestamp".to_string(),
				source: "source".to_string(),
				message: "message".to_string(),
			}
		);

		assert!(actual.is_ascii());
	}

	#[test]
	fn test_serde_request_package() {
		let expected = RequestPackage {
			command: Command::Log(LogMessage {
				service: "service".to_string(),
				target: "target".to_string(),
				level: "level".to_string(),
				timestamp: "timestamp".to_string(),
				source: "source".to_string(),
				message: "message".to_string(),
			}),
		};
		let encoded: Vec<u8> = expected.clone().into();
		let actual: RequestPackage = encoded.into();
		assert_eq!(expected, actual);
	}

	#[test]
	fn test_serde_response_package() {
		let expected = ResponsePackage {
			status: Status::Err("unknown exception".to_string()),
		};
		let encoded: Vec<u8> = expected.clone().into();
		let actual: ResponsePackage = encoded.into();
		assert_eq!(expected, actual);
	}
}
