use crate::protocol::*;
use std::net::SocketAddr;
use tokio::{
	io::{AsyncReadExt, AsyncWriteExt},
	net::{TcpListener, TcpStream},
};

#[derive(Clone)]
pub struct Server<A, B>
where
	A: Fn(AuthToken, SocketAddr) -> bool + Sync + Send + Clone + 'static,
	B: Fn(LogMessage, SocketAddr) -> Status + Sync + Send + Clone + 'static,
{
	on_login: A,
	on_log: B,
}

impl<A, B> Server<A, B>
where
	A: Fn(AuthToken, SocketAddr) -> bool + Sync + Send + Clone + 'static,
	B: Fn(LogMessage, SocketAddr) -> Status + Sync + Send + Clone + 'static,
{
	pub fn new(on_login: A, on_log: B) -> Self {
		Server { on_login, on_log }
	}

	pub async fn listen(&self, host: &str, port: u16) {
		let listener = TcpListener::bind((host, port)).await.unwrap();
		println!("server listening on {host}:{port}");
		loop {
			let server = self.clone();
			match listener.accept().await {
				Err(err) => eprintln!("error on accept tcpstream. err = {}", err),
				Ok((mut stream, addr)) => {
					tokio::spawn(async move {
						let mut authenticated = false;
						loop {
							let server = server.clone();
							let package = next_package(&mut stream).await;
							let package = match package {
								Some(package) => package,
								None => break,
							};
							match package.command {
								Command::NoOp => {
									send_status(&mut stream, Status::NoCmd).await;
								}
								Command::Login(token) => {
									if !authenticated {
										authenticated = (server.on_login)(token, addr);
										if authenticated {
											send_status(&mut stream, Status::Ok).await;
										} else {
											send_status(&mut stream, Status::AuthErr).await;
										}
									}
								}
								Command::Log(message) => {
									if authenticated {
										let status = (server.on_log)(message, addr);
										send_status(&mut stream, status).await;
									} else {
										send_status(&mut stream, Status::AuthErr).await;
									}
								}
							}
						}
						stream.shutdown().await.ok();
					});
				}
			}
		}
	}
}

async fn next_package(stream: &mut TcpStream) -> Option<RequestPackage> {
	loop {
		let size: usize;
		let mut header_bytes = vec![0; 8];
		match stream.read_exact(&mut header_bytes).await {
			Ok(n) => {
				if n == 0 {
					return None;
				}
				match HeaderPackage::try_parse(&header_bytes) {
					Some(header) => size = header.bytes,
					None => continue,
				};
			}
			Err(_) => return None,
		};

		if size > 0 {
			let mut bytes = vec![0; size];
			match stream.read_exact(&mut bytes).await {
				Ok(_) => return Some(bytes.into()),
				Err(_) => return None
			}
		}
	}
}

async fn send_status(stream: &mut TcpStream, status: Status) {
	let package = ResponsePackage { status };
	let bytes: Vec<u8> = package.into();

	let header = HeaderPackage { bytes: bytes.len() };
	let header_bytes: Vec<u8> = header.into();

	if let Err(err) = stream.write_all(&[header_bytes, bytes].concat()[..]).await {
		eprintln!("failed to write to tcpstream. err = {err}");
	}
}

// #[cfg(test)]
// mod tests {
// 	use super::*;

// 	#[test]
// 	fn test() {
// 		assert!(true);
// 	}
// }
