#[cfg(not(any(feature = "postcard", feature = "msgpack", feature = "bincode")))]
compile_error!("no encoding specified, please use exactly one of the following feature flags: postcard, msgpack, bincode.
By default, postcard is enabled. More info here: https://gitlab.com/rokkett/rokkett-logger/-/tree/main/benches.");

#[cfg(any(all(feature = "postcard", feature = "msgpack"), all(feature = "postcard", feature = "bincode"), all(feature = "bincode", feature = "msgpack")))]
compile_error!("more than one encoding specified, please use exactly one of the following feature flags: postcard, msgpack, bincode.
By default, postcard is enabled. More info here: https://gitlab.com/rokkett/rokkett-logger/-/tree/main/benches.");


pub fn serialize<T>(value: &T) -> anyhow::Result<Vec<u8>>
where
	T: ?Sized + serde::Serialize,
{
	#[cfg(feature = "postcard")]
	let bytes = postcard::to_allocvec(&value)?;

	#[cfg(feature = "msgpack")]
	let bytes = rmp_serde::to_vec(&value)?;

	#[cfg(feature = "bincode")]
	let bytes = bincode::serialize(&value)?;

	Ok(bytes)
}

pub fn deserialize<'a, T>(bytes: &'a [u8]) -> anyhow::Result<T>
where
	T: serde::de::Deserialize<'a>,
{
	#[cfg(feature = "postcard")]
	let value: T = postcard::from_bytes(bytes)?;

	#[cfg(feature = "msgpack")]
	let value: T = rmp_serde::from_slice(bytes)?;

	#[cfg(feature = "bincode")]
	let value: T = bincode::deserialize::<'_, T>(bytes)?;

	Ok(value)
}
