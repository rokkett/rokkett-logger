use crate::protocol::{Command, HeaderPackage, RequestPackage, ResponsePackage, Status};
use std::{
	io::{Read, Write},
	net::*,
	time::Duration,
};

const TIMEOUT: Duration = Duration::from_secs(2);

pub struct ClientConnection {
	stream: TcpStream,
}

impl ClientConnection {
	pub fn new(addr: &SocketAddr) -> anyhow::Result<Self> {
		let stream = TcpStream::connect_timeout(&addr, TIMEOUT)?;
		Ok(Self { stream })
	}

	pub fn send(&mut self, package: RequestPackage) -> anyhow::Result<Status> {
		{
			let bytes: Vec<u8> = package.into();
			let header = HeaderPackage { bytes: bytes.len() };
			let header_bytes: Vec<u8> = header.into();

			if let Err(err) = self.stream.write_all(&[header_bytes, bytes].concat()[..]) {
				eprintln!("failed to write to tcpstream. err = {}", err);
			}
		}

		let size: usize;
		let mut header_bytes = vec![0; 8];
		self.stream.read_exact(&mut header_bytes)?;
		match HeaderPackage::try_parse(&header_bytes) {
			Some(header) => size = header.bytes,
			None => return Ok(Status::ParseErr),
		};

		let mut bytes = vec![0; size];
		self.stream.read_exact(&mut bytes)?;
		let response: ResponsePackage = bytes.into();
		Ok(response.status)
	}

	pub fn has_broken(&self) -> bool {
		false
		// self.stream.peek(&mut [0; 0]).is_err() // <- this will wait until another byte is written, so it is no good
	}

	pub fn is_valid(&mut self) -> anyhow::Result<Status> {
		self.send(RequestPackage { command: Command::NoOp })
	}
}
