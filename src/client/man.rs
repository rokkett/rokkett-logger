use r2d2::ManageConnection;
use std::net::SocketAddr;
use thiserror::Error;

use crate::protocol::{RequestPackage, Status, Command};

use super::conn::ClientConnection;

#[derive(Debug)]
pub struct ConnectionManager {
	addr: SocketAddr,
	token: String,
}

impl ConnectionManager {
	pub fn new(addr: SocketAddr, token: String) -> Self {
		ConnectionManager { addr, token }
	}
}

#[derive(Error, Debug)]
pub enum Error {
	#[error("{0}")]
	Any(anyhow::Error),
	#[error("{0}")]
	Status(Status),
}

impl ManageConnection for ConnectionManager {
	type Connection = ClientConnection;
	type Error = Error;

	fn connect(&self) -> Result<Self::Connection, Self::Error> {
		let mut conn = match ClientConnection::new(&self.addr) {
			Ok(conn) => conn,
			Err(err) => return Err(Self::Error::Any(err)),
		};
		let login = RequestPackage {
			command: Command::Login(self.token.to_owned()),
		};
		let status = match conn.send(login) {
			Ok(status) => status,
			Err(err) => return Err(Self::Error::Any(err)),
		};
		match status {
			Status::Ok => Ok(conn),
			Status::NoCmd => Ok(conn),
			status => Err(Self::Error::Status(status)),
		}
	}

	fn is_valid(&self, conn: &mut Self::Connection) -> Result<(), Self::Error> {
		let status = match conn.is_valid() {
			Ok(status) => status,
			Err(err) => return Err(Self::Error::Any(err)),
		};
		match status {
			Status::Ok => Ok(()),
			Status::NoCmd => Ok(()),
			status => Err(Self::Error::Status(status)),
		}
	}

	fn has_broken(&self, conn: &mut Self::Connection) -> bool {
		conn.has_broken()
	}
}
