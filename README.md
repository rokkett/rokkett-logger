# Rokkett Logger

[![Status](https://gitlab.com/rokkett/rokkett-logger/badges/main/pipeline.svg?ignore_skipped=true&key_text=tests&key_width=40)](https://gitlab.com/rokkett/rokkett-logger/-/pipelines)
[![Coverage](https://gitlab.com/rokkett/rokkett-logger/badges/main/coverage.svg)](https://gitlab.com/rokkett/rokkett-logger/-/pipelines)

Lib containing the client and server for a centralized logging setup.

## Client implementation

To use rokkett-logger, add it as a dependency to your `Cargo.toml`.

```bash
$ cargo add rokkett-logger
```

To configure the logger, create a new `RokkettLogger` which shall be initialized only **once** before any logs are generated.

```rust
#[tokio::main]
async fn main() {
	// register logger
	RokkettLogger::new("127.0.0.1:5500", "My App", Some("secret"))
		.expect("failed to create logger");

	// start logging :)
	log::info!("Hello, world!");
}
```

See [examples](https://gitlab.com/rokkett/rokkett-logger/-/tree/main/examples/client) for more examples.

## Server implementation

To use rokkett-logger, add it as a dependency to your `Cargo.toml`.

```bash
$ cargo add rokkett-logger
```

```rust
#[tokio::main]
async fn main() {
	let on_login = |token, _addr| token == "secret".to_string();
	let on_log = |message, _addr| {
		println!("{message}");
		Status::Ok
	};
	Server::new(on_login, on_log).listen("127.0.0.1", 5500).await;
}
```

See [examples](https://gitlab.com/rokkett/rokkett-logger/-/tree/main/examples/server) for more examples.

## License

Rokkett-Logger is distributed under the terms of the [Apache-2.0](./LICENSE) license.  
Find an easy explanation on [choosealicense.com/licenses/apache-2.0](https://choosealicense.com/licenses/apache-2.0/).
