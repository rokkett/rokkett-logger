# run benchmarks

```bash
cargo bench --all-features
```

## bench encodings

| encoding | size  | serialize | deserialize |
| -------- | ----- | --------- | ----------- |
| string   | 123 B | 23 ns     | 83 ns       |
| bincode  | 144 B | 16 ns     | 62 ns       |
| postcard | 123 B | 122 ns    | 59 ns       |
| msgpack  | 125 B | 26 ns     | 73 ns       |

See benchmark soruce in [encodings.rs](encodings.rs).

## simulate network requests

| encoding     | size  | 5Gbps (network/total) | 25Gbps (network/total) |
| ------------ | ----- | --------------------- | ---------------------- |
| bincode      | 224 B | 448 ns / 75 µs        | 90 ns / 67 µs          |
| **postcard** | 176 B | 352 ns / 76 µs        | 70 ns / 70 µs          |
| msgpack      | 183 B | 366 ns / 77 µs        | 73 ns / 70 µs          |

See benchmark soruce in [simulated.rs](simulated.rs).
