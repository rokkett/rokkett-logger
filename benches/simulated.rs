use criterion::*;
use rokkett_logger::*;
use std::time::Duration;

fn dummy() -> NetPackage {
	NetPackage {
		status: Some(Status::Ok),
		command: Some(Command::Log(LogMessage {
			service: env!("CARGO_PKG_NAME").to_string(),
			target: "benchmark".to_string(),
			level: "DEBUG".to_string(),
			timestamp: "2022-10-20T06:51:10:123Z".to_string(),
			source: format!("{}:{}", file!(), line!()),
			message: "Quilava keeps its foes at bay with the intensity of its flames and gusts of superheated air."
				.to_string(),
		})),
	}
}

const MULTIPIER: u64 = 20;

fn net_high_latency(bytes: usize) -> Duration {
	// 5 Gbit/s = 4 Gbit/s usable (tcp/ip error correction)
	// 4 Gbit/s = .5 GB/s = 500 MB/s = 500_000 KB/s = 500_000_000 B/s
	// 500_000_000 B/s = 500_000 B/ms = 500 B/µs = .5 B/ns
	Duration::from_nanos(MULTIPIER * 2 * bytes as u64) // 5 Gbit/s
}

fn net_low_latency(bytes: usize) -> Duration {
	// 25 Gbit/s = 20 Gbit/s usable (tcp/ip error correction)
	// 20 Gbit/s = 2.5 GB/s = 2_500 MB/s = 2_500_000 KB/s = 2_500_000_000 B/s
	// 2_500_000_000 B/s = 2_500_000 B/ms = 2_500 B/µs = 2.5 B/ns
	Duration::from_nanos(MULTIPIER * ((bytes as f64) * 0.4) as u64) // 25 Gbit/s
}

#[cfg(feature = "bincode")]
fn bench_bincode(c: &mut Criterion) {
	let mut group = c.benchmark_group("bincode");
	let data = dummy();
	let bytes = bincode::serialize(&data).unwrap();
	let dur_high = net_high_latency(bytes.len());
	let dur_low = net_low_latency(bytes.len());
	group.bench_function("high lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				bincode::serialize(black_box(&data)).unwrap();
				bincode::deserialize::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_high);
		})
	});
	group.bench_function("low lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				bincode::serialize(black_box(&data)).unwrap();
				bincode::deserialize::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_low);
		})
	});
}

#[cfg(feature = "postcard")]
fn bench_postcard(c: &mut Criterion) {
	let mut group = c.benchmark_group("postcard");
	let data = dummy();
	let bytes = postcard::to_allocvec(&data).unwrap();
	let dur_high = net_high_latency(bytes.len());
	let dur_low = net_low_latency(bytes.len());
	group.bench_function("high lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				postcard::to_allocvec(black_box(&data)).unwrap();
				postcard::from_bytes::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_high);
		})
	});
	group.bench_function("low lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				postcard::to_allocvec(black_box(&data)).unwrap();
				postcard::from_bytes::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_low);
		})
	});
}

#[cfg(feature = "msgpack")]
fn bench_msgpack(c: &mut Criterion) {
	let mut group = c.benchmark_group("msgpack");
	let data = dummy();
	let bytes = rmp_serde::to_vec(&data).unwrap();
	let dur_high = net_high_latency(bytes.len());
	let dur_low = net_low_latency(bytes.len());
	group.bench_function("high lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				rmp_serde::to_vec(black_box(&data)).unwrap();
				rmp_serde::from_slice::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_high);
		})
	});
	group.bench_function("low lat", |b| {
		b.iter(|| {
			for _ in 0..MULTIPIER {
				rmp_serde::to_vec(black_box(&data)).unwrap();
				rmp_serde::from_slice::<'_, NetPackage>(black_box(&bytes)).unwrap();
			}
			std::thread::sleep(dur_low);
		})
	});
}

criterion_group!(benches, bench_bincode, bench_postcard, bench_msgpack);
criterion_main!(benches);
