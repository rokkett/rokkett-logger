use criterion::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
struct Data {
	timestamp: String,
	level: String,
	message: String,
}

impl Data {
	pub fn dummy() -> Self {
		Data {
			timestamp: "2022-10-20T06:51:10:123Z".to_string(),
			level: "INFO".to_string(),
			message: "Quilava keeps its foes at bay with the intensity of its flames and gusts of superheated air."
				.to_string(),
		}
	}

	pub fn to_bytes(&self) -> Vec<u8> {
		[
			self.timestamp.as_bytes(),
			b";",
			self.level.as_bytes(),
			b";",
			self.message.as_bytes(),
			b"\n",
		]
		.concat()
	}

	pub fn from_bytes(bytes: &Vec<u8>) -> Self {
		let mut parts = bytes[..bytes.len() - 2].split(|b| *b == b';');
		Data {
			timestamp: std::str::from_utf8(parts.next().unwrap()).unwrap_or("").to_string(),
			level: std::str::from_utf8(parts.next().unwrap()).unwrap_or("").to_string(),
			message: std::str::from_utf8(parts.next().unwrap()).unwrap_or("").to_string(),
		}
	}
}

fn bench_string(c: &mut Criterion) {
	let mut group = c.benchmark_group("string");
	let data = Data::dummy();
	let bytes = data.to_bytes();
	println!("string: {} bytes", bytes.len());

	group.bench_function("serialize", |b| b.iter(|| black_box(&data).to_bytes()));
	group.bench_function("deserialize", |b| b.iter(|| Data::from_bytes(&bytes)));
}

#[cfg(feature = "bincode")]
fn bench_bincode(c: &mut Criterion) {
	let mut group = c.benchmark_group("bincode");
	let data = Data::dummy();
	let bytes = bincode::serialize(&data).unwrap();
	println!("bincode: {} bytes", bytes.len());

	group.bench_function("serialize", |b| {
		b.iter(|| bincode::serialize(black_box(&data)).unwrap())
	});
	group.bench_function("deserialize", |b| {
		b.iter(|| bincode::deserialize::<'_, Data>(black_box(&bytes)).unwrap())
	});
}

#[cfg(feature = "postcard")]
fn bench_postcard(c: &mut Criterion) {
	let mut group = c.benchmark_group("postcard");
	let data = Data::dummy();
	let bytes = postcard::to_allocvec(&data).unwrap();
	println!("postcard: {} bytes", bytes.len());

	group.bench_function("serialize", |b| {
		b.iter(|| postcard::to_allocvec(black_box(&data)).unwrap())
	});
	group.bench_function("deserialize", |b| {
		b.iter(|| postcard::from_bytes::<'_, Data>(black_box(&bytes)).unwrap())
	});
}

#[cfg(feature = "msgpack")]
fn bench_msgpack(c: &mut Criterion) {
	let mut group = c.benchmark_group("msgpack");
	let data = Data::dummy();
	let bytes = rmp_serde::to_vec(&data).unwrap();
	println!("msgpack: {} bytes", bytes.len());

	group.bench_function("serialize", |b| b.iter(|| rmp_serde::to_vec(black_box(&data)).unwrap()));
	group.bench_function("deserialize", |b| {
		b.iter(|| rmp_serde::from_slice::<'_, Data>(black_box(&bytes)).unwrap())
	});
}

criterion_group!(benches, bench_string, bench_bincode, bench_postcard, bench_msgpack);
criterion_main!(benches);
