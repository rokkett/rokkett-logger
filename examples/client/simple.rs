use std::{thread, time::Duration};

use rokkett_logger::*;

#[tokio::main]
async fn main() {
	RokkettLogger::new("127.0.0.1:5500", "My App", Some("secret")).expect("failed to create logger");

	log::info!("Hello, world!");

	thread::sleep(Duration::from_secs(1)); // wait while logger sends message in background
}
