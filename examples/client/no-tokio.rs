use std::{thread, time::Duration};

use rokkett_logger::*;

// use with "--no-default-features -F client -<encoding> where encoding can be: postcard, msgpack or bincode"

fn main() {
	RokkettLogger::new("127.0.0.1:5500", "My App", None)
		.expect("failed to create logger");

	log::info!("Hello, world!");

	thread::sleep(Duration::from_secs(1)); // wait while logger sends message in background
}
