use rokkett_logger::{protocol::Status, server::Server};

#[tokio::main]
async fn main() {
	let on_login = |token, _addr| token == "secret".to_string();
	let on_log = |message, _addr| {
		println!("{message}");
		Status::Ok
	};
	Server::new(on_login, on_log).listen("127.0.0.1", 5500).await;
}
